Role Name
=========
Установка node_exporter, официальный exporter prometheus для сбора системных метрик.

Requirements
------------
На хосте должен быть открыт порт TCP:9100

Role Variables
--------------
defaults/main.yml - указать последнюю версию. Получить номер версии можно тут
https://github.com/prometheus/node_exporter/releases

Example Playbook
----------------
```yaml
- hosts: all
  become: yes
  gather_facts: yes
  vars:
	  version: 1.1.1
  roles:
    - node_exporter
```

License
-------

MIT

Author Information
------------------

Konstantin Khazov <khazovkv@gmail.com>

